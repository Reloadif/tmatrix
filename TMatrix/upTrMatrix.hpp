#pragma once

//#include <iostream>
//#include <exception>

//#define MAXSIZE 3;

#include "TVector.hpp"

#include "myStreamupTrMatrix.hpp"

template < class Mtype >
class upTrMatrix : protected TVector< TVector<Mtype> > {
	int rowAndColumnUpTrMatrix;

	friend std::istream& operator >> <Mtype> (std::istream& stream, upTrMatrix& obj);
	friend std::ostream& operator << <Mtype> (std::ostream& stream, const upTrMatrix& obj);

public:
	upTrMatrix();
	upTrMatrix(const int _rowAndColumnUpTrMatrix, const Mtype value = 0);
	upTrMatrix(const int _rowAndColumnUpTrMatrix, const TVector< TVector <Mtype> >* vector);
	upTrMatrix(const upTrMatrix& matrix);

	~upTrMatrix();

	int getRowAndColumnUpTrMatrix() const;

	upTrMatrix operator*(const Mtype value);

	upTrMatrix operator+(const upTrMatrix& _matrix);
	upTrMatrix operator-(const upTrMatrix& _matrix);

	TVector<Mtype> operator*(const TVector<Mtype>& vector);
	upTrMatrix operator*(const upTrMatrix& _matrix);

	const bool operator==(const upTrMatrix& _matrix);
	const bool operator!=(const upTrMatrix& _matrix);

	upTrMatrix& operator=(const upTrMatrix& _matrix);

	Mtype& operator()(const int firstIndex, const int secondIndex);
	const Mtype operator()(const int firstIndex, const int secondIndex) const;
};



template < class Mtype >
upTrMatrix<Mtype>::upTrMatrix() : TVector < TVector<Mtype> >() {
	setlocale(LC_ALL, "RUS");

	rowAndColumnUpTrMatrix = MAXSIZE;

	if (this->VArray != nullptr) {
		int j = 0;
		for (int i = rowAndColumnUpTrMatrix; i > 0; i--) {
			this->VArray[j] = TVector<Mtype>(i, j + 1, (Mtype)0);
			j++;
		}
	}
	else throw std::exception("��������� ������ �� ���������.");
}
template < class Mtype >
upTrMatrix<Mtype>::upTrMatrix(const int _rowAndColumnUpTrMatrix, const Mtype value ) : TVector < TVector<Mtype> >(_rowAndColumnUpTrMatrix) {
	setlocale(LC_ALL, "RUS");

	if (_rowAndColumnUpTrMatrix <= 1) {
		throw std::exception("����������������� ������� ������ ����� ����� 1 ������ � 1 �������!");
		rowAndColumnUpTrMatrix = MAXSIZE;
	}
	else {
		rowAndColumnUpTrMatrix = _rowAndColumnUpTrMatrix;
	}

	if (this->VArray != nullptr) {
		int j = 0;
		for (int i = rowAndColumnUpTrMatrix; i > 0; i--) {
			this->VArray[j] = TVector<Mtype>(i, j + 1, value);
			j++;
		}
	}
	else throw std::exception("��������� ������ �� ���������.");
}
template < class Mtype >
upTrMatrix<Mtype>::upTrMatrix(const int _rowAndColumnUpTrMatrix, const TVector< TVector <Mtype> >* vector) : TVector < TVector<Mtype> >(_rowAndColumnUpTrMatrix) {
	setlocale(LC_ALL, "RUS");

	if (_rowAndColumnUpTrMatrix <= 1) {
		throw std::exception("����������������� ������� ������ ����� ����� 1 ������ � 1 �������!");
		rowAndColumnUpTrMatrix = MAXSIZE;
	}
	else {
		rowAndColumnUpTrMatrix = _rowAndColumnUpTrMatrix;
	}

	if (this->VArray != nullptr) {
		for (int i = 0; i < rowAndColumnUpTrMatrix; i++) {
			this->VArray[i] = vector->VArray[i];
		}
	}
	else throw std::exception("��������� ������ �� ���������.");
}
template < class Mtype >
upTrMatrix<Mtype>::upTrMatrix(const upTrMatrix& matrix) : TVector < TVector<Mtype> >(matrix.rowAndColumnUpTrMatrix) {
	setlocale(LC_ALL, "RUS");

	this->sizeVArray = matrix.sizeVArray;
	this->positionInMatrix = matrix.positionInMatrix;
	this->rowAndColumnUpTrMatrix = matrix.rowAndColumnUpTrMatrix;

	if (this->VArray != nullptr) {
		for (int i = 0; i < this->sizeVArray; i++)
			this->VArray[i] = matrix.VArray[i];
	}
	else throw std::exception("��������� ������ �� ���������.");
}


template < class Mtype >
upTrMatrix<Mtype>::~upTrMatrix()   {
	if (this->VArray != nullptr) {
		delete[] this->VArray;
		this->rowAndColumnUpTrMatrix = NULL;
		this->sizeVArray = NULL;
		this->VArray = nullptr;
		this->positionInMatrix = -1;
	}
}


template < class Mtype >
int upTrMatrix<Mtype>::getRowAndColumnUpTrMatrix() const {
	return rowAndColumnUpTrMatrix;
}


template < class Mtype >
upTrMatrix<Mtype> upTrMatrix<Mtype>::operator*(const Mtype value) {
	upTrMatrix<Mtype> tmp(*this);
	for (int i = 0; i < rowAndColumnUpTrMatrix; i++) {
		tmp.VArray[i] = this->VArray[i] * value;
	}
	return tmp;
}


template < class Mtype >
upTrMatrix<Mtype> upTrMatrix<Mtype>::operator+(const upTrMatrix& _matrix) {
	setlocale(LC_ALL, "RUS");

	upTrMatrix<Mtype> tmp(*this);

	if (this->rowAndColumnUpTrMatrix == _matrix.rowAndColumnUpTrMatrix) {
		for (int i = 0; i < rowAndColumnUpTrMatrix; i++) {
			tmp.VArray[i] = this->VArray[i] + _matrix.VArray[i];
		}
	}
	else throw std::exception("����������� ������ �� ���������.");
	return tmp;
}
template < class Mtype >
upTrMatrix<Mtype> upTrMatrix<Mtype>::operator-(const upTrMatrix& _matrix) {
	setlocale(LC_ALL, "RUS");

	upTrMatrix<Mtype> tmp(*this);

	if ( this->rowAndColumnUpTrMatrix == _matrix.rowAndColumnUpTrMatrix ) {
		for (int i = 0; i < rowAndColumnUpTrMatrix; i++) {
			tmp.VArray[i] = this->VArray[i] - _matrix.VArray[i];
		}
	}
	else throw std::exception("����������� ������ �� ���������.");
	return tmp;
}


template < class Mtype >
TVector<Mtype> upTrMatrix<Mtype>::operator*(const TVector<Mtype>& vector) {
	setlocale(LC_ALL, "RUS");

	TVector<Mtype> tmp(vector);
	Mtype result;

	if (this->rowAndColumnUpTrMatrix == vector.getSizeVArray() ) {
		for (int i = 0; i < this->rowAndColumnUpTrMatrix; i++) {
			result = 0;
			for (int j = i; j < this->rowAndColumnUpTrMatrix; j++) {
				result += this->operator()(i, j) * vector[j];
			}
			tmp[i] = result;
		}
	}
	else throw std::exception("����������� ������� � ������� �� ���������.");
	return tmp;
}
template < class Mtype >
upTrMatrix<Mtype> upTrMatrix<Mtype>::operator*(const upTrMatrix& _matrix) {
	setlocale(LC_ALL, "RUS");
	upTrMatrix<Mtype> tmp(*this);

	if (this->rowAndColumnUpTrMatrix == _matrix.rowAndColumnUpTrMatrix) {

		Mtype result;

		for (int i = 0; i < this->rowAndColumnUpTrMatrix; i++) {

			for (int sum = i; sum < this->rowAndColumnUpTrMatrix; sum++) {

				result = 0;
				for (int j = i; j < this->rowAndColumnUpTrMatrix; j++) {
						result += this->operator()(i, j) * _matrix(j, sum);

				}
				tmp(i, sum) = result;
			}

		}
		return tmp;
	}
	else {
		throw std::exception("����������� ������ �� ��������.");
		return tmp;
	}
}


template < class Mtype >
const bool  upTrMatrix<Mtype>::operator==(const upTrMatrix& _matrix) {
	if (this->rowAndColumnUpTrMatrix == _matrix.rowAndColumnUpTrMatrix ) {
		for (int i = 0; i < this->rowAndColumnUpTrMatrix; i++) {
			if (this->VArray[i] != _matrix.VArray[i]) return false;
			return true;
		}
	}
	return false;
}
template < class Mtype >
const bool  upTrMatrix<Mtype>::operator!=(const upTrMatrix& _matrix) {
	return !(*this == _matrix);
}


template < class Mtype >
upTrMatrix<Mtype>& upTrMatrix<Mtype>::operator=(const upTrMatrix<Mtype>& _matrix) {
	setlocale(LC_ALL, "RUS");

	if (this != &_matrix) {
		if (this->sizeVArray != _matrix.sizeVArray) {
			for (int i = 0; i < this->sizeVArray; i++) {
				TVector<Mtype>& tmp = this->VArray[i];
				tmp.~TVector();
			}
			delete[] this->VArray;

			this->rowAndColumnUpTrMatrix = _matrix.rowAndColumnUpTrMatrix;
			this->sizeVArray = _matrix.sizeVArray;
			this->positionInMatrix = _matrix.positionInMatrix;
			this->VArray =  new TVector<Mtype>[this->sizeVArray];
		}

		if (this->VArray != nullptr) {
			this->positionInMatrix = _matrix.positionInMatrix;
			for (int i = 0; i < this->sizeVArray; i++) {
				this->VArray[i] = _matrix.VArray[i];
			}
		}
		else throw std::exception("��������� ������ �� ���������.");
	}
	return *this;
}


template < class Mtype >
const Mtype upTrMatrix<Mtype>:: operator()(const int firstIndex, const int secondIndex) const {
	setlocale(LC_ALL, "RUS");
	TVector<Mtype>& tmp = this->VArray[0];
	Mtype& result = tmp[0];

	if ((firstIndex < 0 || firstIndex >= rowAndColumnUpTrMatrix) && (secondIndex < 0 || secondIndex >= rowAndColumnUpTrMatrix)) {
		throw std::exception("�������� ����� �������� � �������, ������� ������� [1][1].");
		TVector<Mtype>& tmp = this->VArray[0];
		result = tmp[0];
		return result;
	}
	else {
		TVector<Mtype>& tmp = this->VArray[firstIndex];
		int counterOfNull = tmp.getPositionInMatrix() - 1;

		if (counterOfNull > secondIndex) {
			return Mtype(0);
		}
		else return tmp[secondIndex - counterOfNull];
	}
}
template < class Mtype >
Mtype& upTrMatrix<Mtype>:: operator()(const int firstIndex, const int secondIndex) {
	setlocale(LC_ALL, "RUS");
	TVector<Mtype>& tmp = this->VArray[0];
	Mtype& result =  tmp[0];

	if ((firstIndex < 0 || firstIndex >= rowAndColumnUpTrMatrix) && (secondIndex < 0 || secondIndex >= rowAndColumnUpTrMatrix)) {
		throw std::exception("�������� ����� �������� � �������, ������� 0.");
		TVector<Mtype>& tmp = this->VArray[0];
		result = tmp[0];
		return result;
	}
	else {
		TVector<Mtype>& tmp = this->VArray[firstIndex];
		int counterOfNull = tmp.getPositionInMatrix() - 1;

		if (counterOfNull > secondIndex) {
			throw std::exception("������ 0, ������ ���� ����������� � [1][1].");
			TVector<Mtype>& tmp = this->VArray[0];
			result = tmp[0];
			return result;
		}
		else return tmp[secondIndex - counterOfNull];
	}
}