#pragma once
#include <iostream>
#include "TVector.hpp"

template < class Mtype >
class upTrMatrix;

template < class Mtype >
std::istream& operator >> (std::istream& stream, upTrMatrix<Mtype>& obj) {
	Mtype element;
	int rowsAndColumns;

	do {
		std::cout << "������� ���������� ����� � �������� � ����������������� ������� : ";
		stream >> rowsAndColumns;
	} while (rowsAndColumns <= 1);

	upTrMatrix<Mtype> userMatrix(rowsAndColumns);

	for (int i = 0; i < rowsAndColumns; i++) {
		for (int j = i ; j < rowsAndColumns; j++) {
			system("cls");
			std::cout << "������� ������� [" << i+1 << "][" << j+1 << "] : ";
			stream >> element;
			userMatrix(i,j) = element;
		}
	}
	obj = userMatrix;
	return stream;
}
template < class Mtype >
std::ostream& operator << (std::ostream& stream, const upTrMatrix<Mtype>& obj) {
	system("cls");

	for (int i = 0; i < obj.rowAndColumnUpTrMatrix; i++) {
		stream << "(";
		for (int j = 0; j < obj.rowAndColumnUpTrMatrix; j++) {
			stream << obj(i, j) ;
			if ( j != (obj.rowAndColumnUpTrMatrix - 1) ) {
				stream << "\t";
			}
		}
		stream << ")" << std::endl ;
	}
	stream << std::endl;

	system("pause");
	return stream;
}